import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-order-placed',
  templateUrl: './order-placed.component.html',
  styleUrls: ['./order-placed.component.scss'],
})
export class OrderPlacedComponent implements OnInit {

  constructor(private modalController: ModalController) { }

  ngOnInit() {}

  selectUser(){
    this.modalController.dismiss('deliver');
  }

}
