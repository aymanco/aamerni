import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-add-credit-card',
  templateUrl: './add-credit-card.component.html',
  styleUrls: ['./add-credit-card.component.scss'],
})
export class AddCreditCardComponent implements OnInit {

  constructor(private modalController: ModalController) { }

  ngOnInit() {}
  selectUser() {
    this.modalController.dismiss('add');
  }
}
