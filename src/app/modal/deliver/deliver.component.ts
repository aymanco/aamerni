import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-deliver',
  templateUrl: './deliver.component.html',
  styleUrls: ['./deliver.component.scss'],
})
export class DeliverComponent implements OnInit {

  constructor(private modalController: ModalController) { }

  ngOnInit() {}
  selectUser() {
    this.modalController.dismiss('deliverWay');
  }
}
