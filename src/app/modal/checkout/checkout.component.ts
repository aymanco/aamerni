import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss'],
})
export class CheckoutComponent implements OnInit {
  @Input() placeOrder: string;
  disabled = true ;
  constructor(private modalController: ModalController) { }

  ngOnInit() {
    if (this.placeOrder === 'add') {
      this.disabled = false ;
    }
  }
  selectUser() {
    this.modalController.dismiss('card');
  }
  OrderPlaced() {
    this.modalController.dismiss('placeOrder');
  }
}
