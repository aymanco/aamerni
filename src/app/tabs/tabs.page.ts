import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage implements OnInit {
  tab1 = 'assets/icon/tab1.png' ;
  tab2 = 'assets/icon/tab2.png' ;
  tab3 = 'assets/icon/tab3.png' ;
  tab4 = 'assets/icon/tab4.png' ;
  tab5 = 'assets/icon/tab5.png' ;

  constructor() {}
  ngOnInit() {
    console.log(window.location.pathname);
    console.log(window.location.pathname.toString().indexOf('tab1'));
    if (window.location.pathname.toString().indexOf('tab1') > 0 ) {
      this.tab1 = 'assets/icon/tab1a.png' ;
    } else if (window.location.pathname.toString().indexOf('tab2') > 0 ) {
      this.tab2 = 'assets/icon/tab2a.png' ;
    } else if (window.location.pathname.toString().indexOf('tab3') > 0 ) {
      this.tab3 = 'assets/icon/tab3a.png' ;
    } else if (window.location.pathname.toString().indexOf('tab4') > 0 ) {
      this.tab4 = 'assets/icon/tab4a.png' ;
    } else if (window.location.pathname.toString().indexOf('tab5') > 0 ) {
      this.tab5 = 'assets/icon/tab5a.png' ;
    }
  }

  activ(val) {
    this.tab1 = 'assets/icon/tab1.png' ;
    this.tab2 = 'assets/icon/tab2.png' ;
    this.tab3 = 'assets/icon/tab3.png' ;
    this.tab4 = 'assets/icon/tab4.png' ;
    this.tab5 = 'assets/icon/tab5.png' ;
    if (val === 1) {
      this.tab1 = 'assets/icon/tab1a.png' ;
    } else if (val === 2) {
      this.tab2 = 'assets/icon/tab2a.png' ;
    } else if (val === 3 ) {
      this.tab3 = 'assets/icon/tab3a.png' ;
    } else if (val === 4 ) {
      this.tab4 = 'assets/icon/tab4a.png' ;
    } else if (val === 5) {
      this.tab5 = 'assets/icon/tab5a.png' ;
    }

  }
}
