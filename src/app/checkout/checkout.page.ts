import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AddCreditCardComponent } from '../modal/add-credit-card/add-credit-card.component';
import { CheckoutComponent } from '../modal/checkout/checkout.component';
import { DeliverWayComponent } from '../modal/deliver-way/deliver-way.component';
import { DeliverComponent } from '../modal/deliver/deliver.component';
import { OrderPlacedComponent } from '../modal/order-placed/order-placed.component';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.page.html',
  styleUrls: ['./checkout.page.scss'],
})
export class CheckoutPage implements OnInit {
  thanks = 0 ;
  constructor(public modalController: ModalController) { }

  ngOnInit() {

  }

  async presentModal(add) {
    const modal = await this.modalController.create({
      component: CheckoutComponent,
      cssClass: 'my-custom-modal',
      swipeToClose: true,
      componentProps: {
        placeOrder: add,
      }
    });
    modal.onDidDismiss()
      // tslint:disable-next-line: no-shadowed-variable
      .then(async (data: any) => {
        console.log(data);
        if (data.data === 'card') {
          this.presentModalCard('add');
        } else if (data.data === 'placeOrder') {
          this.presentModalOrderPlacerd();
        }
      });
    return await modal.present();
  }

  async presentModalCard(add) {
    const modal = await this.modalController.create({
      component: AddCreditCardComponent,
      cssClass: 'my-custom-modal-card'
    });
    modal.onWillDismiss()
      // tslint:disable-next-line: no-shadowed-variable
      .then(async (data: any) => {
        console.log(data);
        if (data.data === 'add') {
          this.presentModal('add');
        }
      });
    return await modal.present();
  }

  async presentModalOrderPlacerd() {
    const modal = await this.modalController.create({
      component: OrderPlacedComponent,
      cssClass: 'my-custom-modal-order',
    });
    this.thanks = 1;
    modal.onWillDismiss()
      // tslint:disable-next-line: no-shadowed-variable
      .then(async (data: any) => {
        console.log(data);
        if (data.data === 'deliver') {
          this.presentModalDeliver();
        }
      });
    return await modal.present();

  }

  async presentModalDeliver() {
    const modal = await this.modalController.create({
      component: DeliverComponent,
      cssClass: 'my-custom-modal-deliver',
    });
    this.thanks = 0 ;
    modal.onWillDismiss()
    // tslint:disable-next-line: no-shadowed-variable
    .then(async (data: any) => {
      console.log(data);
      if (data.data === 'deliverWay') {
        this.presentModalDeliverWay();
      }
    });
    return await modal.present();

  }

  async presentModalDeliverWay() {
    const modal = await this.modalController.create({
      component: DeliverWayComponent,
      cssClass: 'my-custom-modal-order',
    });
    this.thanks = 2 ;
    return await modal.present();

  }

}
