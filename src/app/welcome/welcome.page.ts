import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.page.html',
  styleUrls: ['./welcome.page.scss'],
})
export class WelcomePage implements OnInit {
  selected = 'assets/icon/tab.png' ;
  country: any = [
    { id: 682 , nom: 'Saudi Arabia', icon: '../../assets/icon/sa.png' },
    { id: 414 , nom: 'Kuwait', icon: '../../assets/icon/kuwait.png' },
    { id: 784 , nom: 'UAE', icon: '../../assets/icon/uae.png' },
    { id: 634 , nom: 'Qatar', icon: '../../assets/icon/qatar.png' },
    { id: 48 , nom: 'Bahrain', icon: '../../assets/icon/bahrain.png' },
    { id: 512 , nom: 'Oman', icon: '../../assets/icon/oman.png' },
    { id: 400 , nom: 'Jordan', icon: '../../assets/icon/jordan.png' },
    { id: 818 , nom: 'Egypt', icon: '../../assets/icon/egypt.png' }
  ];
  active = false;
  state: any;
  constructor(private router: Router) { }

  ngOnInit() {
  }

  onClick(val) {
    this.state = val ;
    this.country.forEach((value, index) => {
      this.selected = 'assets/icon/tabS.png' ;
      const item = document.getElementById(value.id);
      const label = document.getElementById( 'label' +  value.id);
      item.classList.add('click');
      label.classList.add('label');
      if (value.id !== val ) {
        item.classList.remove('click');
        label.classList.remove('label');
      } else {
        item.classList.add('click');
        label.classList.add('label');
      }
    });
  }

  city() {
    this.router.navigate(['/city/' + this.state]);
  }

}
