import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-city',
  templateUrl: './city.page.html',
  styleUrls: ['./city.page.scss'],
})
export class CityPage implements OnInit {
  selected = 'assets/icon/tab.png';
  city: any = [
    { id: 1, nom: 'AL Riyadh', icon: '../../assets/icon/sa.png' },
    { id: 2, nom: 'Al Khobar', icon: '../../assets/icon/kuwait.png' },
    { id: 3, nom: 'Ad Dammam', icon: '../../assets/icon/uae.png' },
    { id: 4, nom: 'Jeddah', icon: '../../assets/icon/qatar.png' },
    { id: 5, nom: 'Mecca', icon: '../../assets/icon/bahrain.png' },
    { id: 6, nom: 'Al Madenah', icon: '../../assets/icon/oman.png' },
    { id: 7, nom: 'Tabouk', icon: '../../assets/icon/jordan.png' },
    { id: 8, nom: 'Al jubail', icon: '../../assets/icon/egypt.png' },
    { id: 9, nom: 'Al Qassim', icon: '../../assets/icon/egypt.png' }
  ];
  delegation: any = [
    { id: 1, nom: 'Abdullah Fuad' },
    { id: 2, nom: 'Al Adamah' },
    { id: 3, nom: 'Al Amamrah' },
    { id: 4, nom: 'Al Anood' },
  ];
  state: any;
  CitySlected: any;
  constructor(private routing: ActivatedRoute,
              private service: ApiService,
              private router: Router) { }

  async ngOnInit() {
    this.routing.paramMap.subscribe((rest: any) => {
      console.log(rest.params.id);
      this.state = rest.params.id;
    });
    /* await this.service.states(this.state).toPromise()
      .then((rest: any) => {
        console.log(rest);
      }); */
  }

  async onClick(val) {
    /* await this.service.city(this.state).toPromise()
      .then((rest: any) => {
        console.log(rest);
      }); */
    this.city.map((value, index) => {
      const item = document.getElementById(value.id);
      const del = document.getElementById('del' + value.id);
      const label = document.getElementById('label' + value.id);

      // tslint:disable-next-line: radix
      if (parseInt(value.id) === parseInt(val)) {
        item.classList.add('click');
        label.classList.add('label');
        del.style.display = 'block ';
      } else {
        item.classList.remove('click');
        label.classList.remove('label');
        del.style.display = 'none ';

      }

    });
  }

  choose(val) {
    this.CitySlected = val;
    this.selected = 'assets/icon/tabS.png';
    this.delegation.map((value, index) => {
      const element = document.getElementById('city' + value.id);
      const img = document.getElementById('check' + value.id);
      element.style.fontWeight = 'blod';

      // tslint:disable-next-line: radix
      if (parseInt(value.id) === parseInt(val)) {
        element.style.fontWeight = 'bold';
        img.style.display = 'block';
      } else {
        element.style.fontWeight = 'normal';
        img.style.display = 'none';

      }
    });
  }
  Done() {
    if (this.selected === 'assets/icon/tabS.png') {
    this.router.navigate(['/notif']);
  }
  }
}
