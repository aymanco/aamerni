import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
  password = '';
  constructor() { }

  ngOnInit() {
  }

  mdp(val) {
    const element = document.getElementById('password-input');
    // tslint:disable-next-line: triple-equals
    if (this.password.length > 0 ) {
        element.classList.add('password');
        console.log(this.password);
      } else {
        element.classList.remove('password');

      }
  }

}
