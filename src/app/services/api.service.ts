import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ApiService {
  URL = 'https://aamerni.com/' ;
  constructor(private http: HttpClient) { }

  login(email , password) {
    return this.http.post(this.URL + 'api/auth/login' , {
      email,
      password
    });
  }
  states(id) {
    return this.http.get(this.URL + 'api/states/' + id ) ;
  }

  city(id) {
    return this.http.get(this.URL + 'api/cities/' + id);
  }
}
