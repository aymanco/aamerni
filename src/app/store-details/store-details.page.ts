import { Component, OnInit } from '@angular/core';
import { ModalController, PopoverController } from '@ionic/angular';
import { ItemComponent } from '../item/item.component';

@Component({
  selector: 'app-store-details',
  templateUrl: './store-details.page.html',
  styleUrls: ['./store-details.page.scss'],
})
export class StoreDetailsPage implements OnInit {
  slideOpts = {
    initialSlide: 1,
    speed: 400
  };

  list = [ { nom: 'Thawb (Summer)' , option1: 0 ,  option2: 0 ,  option3: 0   } ,
           { nom: 'Thawb (Winter)' , option1: 0 ,  option2: 0 ,  option3: 0   } ,
           { nom: 'Gutrah' ,         option1: 0 ,  option2: 0 ,  option3: 0   } ,
           { nom: 'Shimagh' , option1: 0 ,  option2: 0 ,  option3: 0   } ,
           { nom: 'Abaya' , option1: 0 ,  option2: 0 ,  option3: 0   } ,
           { nom: 'Besht' , option1: 0 ,  option2: 0 ,  option3: 0   } ] ;
  top = [ { nom: 'Blouse ' , option1: 0 ,  option2: 0 ,  option3: 0   } ,
          { nom: 'Shirt ' , option1: 0 ,  option2: 0 ,  option3: 0   } ,
          { nom: 'Gutrah' , option1: 0 ,  option2: 0 ,  option3: 0   } ,
          { nom: 'Shimagh' , option1: 0 ,  option2: 0 ,  option3: 0   } ,
          { nom: 'Abaya' , option1: 0 ,  option2: 0 ,  option3: 0   } ,
          { nom: 'Besht' , option1: 0 ,  option2: 0 ,  option3: 0   } ] ;
  constructor(public modalController: ModalController,
              public popoverCtrl: PopoverController,
    ) { }

  ngOnInit() {

  }

   async presentModal() {
    const modal = await this.modalController.create({
      component: ItemComponent,
      cssClass: 'my-custom-modal-css'
    });
    return await modal.present();
  }
}

