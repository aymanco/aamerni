import { Component, OnInit } from '@angular/core';
import { Environment, Geocoder, GeocoderResult, GoogleMap, GoogleMapOptions, GoogleMaps } from '@ionic-native/google-maps';
import { ModalController } from '@ionic/angular';
import { AddressComponent } from '../address/address.component';

@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit {
  map: GoogleMap;

  constructor(public modalController: ModalController) { }

  ngOnInit() {
    this.loadMap();
  }

  async loadMap() {

    const mapOptions: GoogleMapOptions = {
      camera: {
        target: {
          lat: 43.0741904,
          lng: -89.3809802
        },
        zoom: 18,
        tilt: 30
      }
    };

    this.map = GoogleMaps.create('map_canvas', mapOptions);
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: AddressComponent,
      cssClass: 'my-custom-modal-css'
    });
    return await modal.present();
  }

}
