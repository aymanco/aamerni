import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  password = '';
  constructor(private services: ApiService) { }

  ngOnInit() {
  }

  mdp(val) {
    const element = document.getElementById('password-input');
    // tslint:disable-next-line: triple-equals
    if (this.password.length > 0 ) {
        element.classList.add('password');
        console.log(this.password);
      } else {
        element.classList.remove('password');

      }
  }
  async login() {
    await this.services.login('customer@demo.com' , '123456').subscribe( (rest: any) => {
      console.log(rest);
    });
  }
}
